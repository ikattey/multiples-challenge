package com.ikattey.multiples;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ikattey.multiples.domain.MaxNumber;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class AppControllerTest {
    final int defaultMaxNumber = 10;

    @Autowired
    private MockMvc mockMvc;


    @Test
    void updateMaxNumber_ValidRequest_ShouldChangeConfig() throws Exception {

        // first assert default max
        mockMvc.perform(get("/max")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.value", equalTo(defaultMaxNumber)));


        // then send update request
        mockMvc.perform(put("/max")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(new MaxNumber(25 % defaultMaxNumber))))
                .andExpect(status().isOk());

        // assert update
        mockMvc.perform(get("/max")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.value", equalTo(25 % defaultMaxNumber)));

    }


    @Test
    void updateMaxNumber_InvalidValue_ShouldReturnError() throws Exception {
        // then send update request
        mockMvc.perform(put("/max")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(new MaxNumber(26))))
                .andExpect(status().isBadRequest());
    }

    @Test
    void getCalculation_JSONRequestHeader_ShouldReturnJSON() throws Exception {
        // checks calculation using default max of 10
        mockMvc.perform(get("/")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", equalTo(2520)))
                .andExpect(jsonPath("$.durationMillis", any(Integer.class)))
                .andExpect(jsonPath("$.maxNumber", equalTo(defaultMaxNumber)));
    }

    @Test
    void getCalculation_WithSpecifiedMaxNumber_ShouldReturnCorrectly() throws Exception {
        mockMvc.perform(get("/?max=5")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", equalTo(60)))
                .andExpect(jsonPath("$.durationMillis", any(Integer.class)))
                .andExpect(jsonPath("$.maxNumber", equalTo(5)));
    }

    @Test
    void getCalculation_XMLRequestHeader_ShouldReturnXML() throws Exception {
        // checks calculation using default max of 10
        mockMvc.perform(get("/")
                .accept(MediaType.APPLICATION_XML_VALUE))
                .andExpect(status().isOk())
                .andExpect(xpath("/response/result/text()").string(equalTo("2520")))
                .andExpect(xpath("/response/maxNumber/text()").string(equalTo("10")))
                .andExpect(xpath("/response/durationMillis/text()").string(anything()));
    }

    /**
     * Helper method to create JSON strings for requests
     */
    private String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}