package com.ikattey.multiples.services;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculationServiceImplTest {

    CalculationServiceImpl calc = new CalculationServiceImpl();

    @Test
    void testGreatestCommonDivisor() {
        assertAll("Greatest common divisor",
                () -> assertEquals(3, calc.getGreatestCommonDivisor(9, 15)), // common prime divisor
                () -> assertEquals(1, calc.getGreatestCommonDivisor(7, 18)), // no common prime divisor
                () -> assertEquals(1, calc.getGreatestCommonDivisor(1, 18)), // "1" in input
                () -> assertEquals(4, calc.getGreatestCommonDivisor(4, 12))   // inputs are divisible
        );
    }

    @Test
    void testLeastCommonMultiple() {
        assertAll("Least common multiple",
                () -> assertEquals(20, calc.getLeastCommonMultiple(4, 5)),
                () -> assertEquals(546, calc.getLeastCommonMultiple(2, 3, 7, 13)),
                () -> assertEquals(15, calc.getLeastCommonMultiple(1, 3, 5)),
                () -> assertEquals(60, calc.getLeastCommonMultiple(3, 10, 12))
        );
    }
}