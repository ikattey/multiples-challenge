package com.ikattey.multiples.services;

public interface CalculationService {
    /**
     * Computes the least common multiple of a list of numbers, provided as arguments to the method.
     *
     * @param numbers list of numbers
     * @return the result
     */
    long getLeastCommonMultiple(long... numbers);
}
