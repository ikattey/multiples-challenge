package com.ikattey.multiples.services;

import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class CalculationServiceImpl implements CalculationService {


    /**
     * Computes the greatest common divisor of its arguments, using Euclid's algorithm.
     * @param a the first value
     * @param b the second value
     * @return the result
     */
    protected long getGreatestCommonDivisor(long a, long b) {
      return  (b == 0 )? a : getGreatestCommonDivisor(b, a % b);
    }


    /**
     * Computes the least common multiple of a list of numbers, using the greatest common divisor.
     * For two integers a and b, LCM(a, b) = (a x b) / GCD(a, b).
     * LCM is associative, ie. LCM(a,b,c) = LCM(LCM(a, b), C).
     *
     * @param numbers list of numbers
     * @return the result
     */
    public long getLeastCommonMultiple(long... numbers){
        return Arrays.stream(numbers).reduce(1L, (a, b) -> Math.abs(a * b) / getGreatestCommonDivisor(a, b));
    }
}
