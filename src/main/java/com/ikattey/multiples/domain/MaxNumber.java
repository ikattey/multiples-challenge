package com.ikattey.multiples.domain;

/**
 * Represents the maximum number in the sequence used in calculations across
 * the application.
 */
public class MaxNumber {
    private int value;

    public MaxNumber() {
    }

    public MaxNumber(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
