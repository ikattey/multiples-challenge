package com.ikattey.multiples.domain;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

/**
 * Generic class for wrapping API responses.
 * Wraps XML data with <response></response> tags
 */
@JacksonXmlRootElement(localName = "response")
public class ApiResponse {

    private long result;
    private long durationMillis;
    private int maxNumber;

    public ApiResponse() {
    }

    public ApiResponse(long result, long durationMillis, int maxNumber) {
        this.result = result;
        this.durationMillis = durationMillis;
        this.maxNumber = maxNumber;
    }

    public long getResult() {
        return result;
    }

    public long getDurationMillis() {
        return durationMillis;
    }

    public int getMaxNumber() {
        return maxNumber;
    }

    public void setMaxNumber(int maxNumber) {
        this.maxNumber = maxNumber;
    }
}
