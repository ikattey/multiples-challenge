package com.ikattey.multiples;

import com.ikattey.multiples.domain.MaxNumber;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Clock;

@Configuration
public class AppConfig {
    private final int DEFAULT_MAX_NUMBER = 10;

    /**
     * Keeps track of the currently configured max number for number sequence to use in calculating
     * least common multiple.
     */
    @Bean
    public MaxNumber maxNumberConfig() {
        return new MaxNumber(DEFAULT_MAX_NUMBER);
    }

    /**
     * Gets clock instance used within application
     */
    @Bean
    public Clock clock() {
        return Clock.systemUTC();
    }
}
