package com.ikattey.multiples;

import com.ikattey.multiples.domain.ApiResponse;
import com.ikattey.multiples.domain.MaxNumber;
import com.ikattey.multiples.services.CalculationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.Optional;
import java.util.stream.LongStream;

@RestController
@RequestMapping("/")
public class AppController {

    private final MaxNumber maxNumberConfig;
    private final CalculationService calc;
    private final Clock clock;
    private final Logger logger = LoggerFactory.getLogger(AppController.class);



    public AppController(MaxNumber maxNumberConfig, CalculationService calc, Clock clock) {
        this.maxNumberConfig = maxNumberConfig;
        this.calc = calc;
        this.clock = clock;
    }

    /**
     * Gets the least common multiple of the sequence of numbers between 1 and the current max number configured in the
     * application.
     * Return type depends on the value of the 'Accepts' header in the request (ie. application/xml or application/json).
     * @return new XML or JSON object containing the result and duration of calculation in milliseconds.
     *
     */
    @GetMapping(value = "/", produces = { MediaType. APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ApiResponse calculateLcm(@RequestParam Optional<Integer> max) {
        int queryMaxNumber = max.orElse(maxNumberConfig.getValue());

        logger.debug("Calculating lowest common multiple from 1 to {}", queryMaxNumber);

        Instant startTime = Instant.now(clock);
        long result = calc.getLeastCommonMultiple(LongStream.rangeClosed(1, queryMaxNumber).toArray());
        long duration = Duration.between(startTime, Instant.now(clock)).toMillis();

        logger.debug("Finished calculation after {} ms; result={}",duration, result);
        return new ApiResponse(result, duration, queryMaxNumber);
    }

    @GetMapping("/max")
    public MaxNumber getCurrentMaxNumber() {
        return maxNumberConfig;
    }

    @PutMapping("/max")
    public ResponseEntity<?> setMaxNumber(@RequestBody MaxNumber newMax) {
        int requestValue = newMax.getValue();
        if(requestValue < 1 || requestValue > 25) {
            logger.info("Received invalid max number in PUT request. value={}", requestValue);
            return new ResponseEntity<>(
                    "max number value must be between 1 and 25",
                    HttpStatus.BAD_REQUEST);
        }
        maxNumberConfig.setValue(newMax.getValue());
        logger.info("Updated configured max number to {}", requestValue);
        return new ResponseEntity<>(maxNumberConfig, HttpStatus.OK);
    }
}
