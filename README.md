# Lowest common multiple challenge
This is a Spring Boot application providing a REST API for finding the smallest positive number
that can be divided by a sequential range of numbers without remainder, starting from 1.

## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org). 

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `com.ikattey.App` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```
The application runs on `localhost:8080` by default.

## Run the tests
Tests can be executed by running the following command from the project directory

    mvn test

# REST API

The REST API to the app is described below.


## Get current max number
To get the configured max number in the sequence, issue the following request

### Request
`GET /max`

    curl -i -H 'Accept: application/json' http://localhost:8080/max
    
### Response
The default max number is 10. 

    HTTP/1.1 200
    Content-Type: application/json
    Transfer-Encoding: chunked
    Date: Sat, 04 Jul 2020 16:00:23 GMT
   
    {"value":10}
       
## Update the max number

### Request
Issue a PUT request to `/max` to set the configured max number. This value must be between 1 and 25.

`PUT /max`

    curl -X PUT -H "Content-Type: application/json" -d '{"value":20}' localhost:8080/max


## Get lowest common multiple 
To compute the lowest common multiple of numbers from 1 to the currently configured max, issue the following GET requests to `localhost:8080/`. 

Responses from this endpoint contain the result and the duration of computation in milliseconds.

### Request (JSON)
Include `Accept: application/json` in request header


    curl -i -H 'Accept: application/json' http://localhost:8080/

### Response 

    HTTP/1.1 200
    Content-Type: application/json
    Transfer-Encoding: chunked
    Date: Sat, 04 Jul 2020 16:13:11 GMT
    
    {"result":18044195,"durationMillis":1,"maxNumber": 10}
    
    
### Request (XML)
Include `Accept: application/xml` in request header


    curl -i -H 'Accept: application/xml' http://localhost:8080/

### Response 

    HTTP/1.1 200
    Content-Type: application/xml
    Transfer-Encoding: chunked
    Date: Sat, 04 Jul 2020 16:17:17 GMT
    
    <response>
       <result>18044195</result>
       <durationMillis>0</durationMillis>
       <maxNumber>10</maxNumber>
    </response>
    
 ### Request (with max param)
 A `max` param can also be included with the GET request to specify how many numbers should be included in the sequence. This value is only used within the scope of the request, and does not change the configured max.
 
     curl -i -H 'Accept: application/json' http://localhost:8080/?max=30
 
 ### Response 
 
     HTTP/1.1 200
     Content-Type: application/json
     Transfer-Encoding: chunked
     Date: Sat, 04 Jul 2020 16:30:17 GMT
     
     {"result":12361388,"durationMillis":1,"maxNumber":30}
     